//-----------------------------------------------------------------------------
// itemfactory.h
// ItemFactory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// ItemFactory class:
//
// Functionality includes:
//    --
//
// Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef ITEMFACTORY_H
#define ITEMFACTORY_H
#include "factory.h"

class ItemFactory : public Factory {

public:
  ItemFactory(void);  // constructor

}; // end ItemFactory

#endif
