//-----------------------------------------------------------------------------
// factory.cpp
// Factory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Factory class: Generic factory super class, for creating factory objects and
//                returning a pointer to the new object. Creates an object by
//                utilizing a hash function. Hashing creates a subscript for
//                an array, which points to an object. Objects must implement
//                create(). Create() is called on the object and returned to
//                user.
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "factory.h"
#include "factoryobject.h"
#include <stddef.h>
#include <ostream>

//-----------------------------------------------------------------------------
// Constructor
Factory::Factory(void) {
  for(int i = 0; i < FACTORY_ARRAY_SIZE; i++)
    factoryObject[i] = NULL;                   // initialize array to NULL
}

//-----------------------------------------------------------------------------
// Destructor
Factory::~Factory(void) {
  for(int i = 0; i < FACTORY_ARRAY_SIZE; i++) {
    delete factoryObject[i];
    factoryObject[i] = NULL;                   // free memory allocation
  }
}

//-----------------------------------------------------------------------------
// Create Object
FactoryObject* Factory::createObject(const char c) const {
  int subscript = hash(c);                  // hash table array subscript
  if (subscript >= 0 &&
        subscript < FACTORY_ARRAY_SIZE &&
          factoryObject[subscript] != NULL)    // if subscript in array && !NULL
    return factoryObject[subscript]->create(); // return object
  return NULL;
}

//-----------------------------------------------------------------------------
// Hash Function
const int Factory::hash(const char c) const {
  return c - 'A';                           // simple hash, mapping alphabet
}
