//-----------------------------------------------------------------------------
// display.h
// Display class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Display class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef DISPLAY_H
#define DISPLAY_H
#include "command.h"

const int AVAIL_COL_WIDTH = 6;
const int AUTHOR_COL_WIDTH = 25;
const int TITLE_COL_WIDTH = 35;
const int YEAR_COL_WIDTH = 4;

class Display : public Command {

public:
	Display(const char = 'D');

  virtual FactoryObject* create(void) const;
	virtual const bool populate(istream&) {}
  // virtual string getUnique(void) const {}
  virtual ostream& run(ostream&,LibraryManager&);

}; // end Display
#endif
