//-----------------------------------------------------------------------------
// return.h
// Return class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Return class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef RETURN_H
#define RETURN_H
#include "command.h"

class Return : public Command {

public:
  Return(void);

  virtual FactoryObject* create(void) const;
  virtual const bool populate(istream&);
  // virtual string getUnique(void) const {}
  virtual ostream& run(ostream&,LibraryManager&);

}; // end Return

#endif
