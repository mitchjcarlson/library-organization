//-----------------------------------------------------------------------------
// list.h
// List class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// List class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef LIST_H
#define LIST_H
#include <cstdlib>
#include <iostream>
using namespace std;

class Item;

class List {

public:
  List(Item* = NULL,string = "");
  virtual ~List(void);
  virtual const bool pushFront(Item*,string);
  virtual ostream& print(ostream&) const;

protected:
  struct Node {
    Node(Item* item, string type) : item(item),type(type),next(NULL) {};
    Item* item;
    string type;
    Node* next;
  };

  Node* front;
  virtual void makeEmpty(Node*);

}; // end List
#endif
