//-----------------------------------------------------------------------------
// command.h
// Command class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Command class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef COMMAND_H
#define COMMAND_H
#include "factoryobject.h"
#include "librarymanager.h"

class HashMap;

class Command : public FactoryObject {

public:
  Command(const char type = '\0') : FactoryObject(type) {}

  virtual ostream& run(ostream&,LibraryManager&) = 0;

}; // end Command

#endif
