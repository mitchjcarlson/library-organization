//-----------------------------------------------------------------------------
// hashmap.cpp
// HashMap class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// HashMap class:
//
//  Functionality includes:
//    --
//
//  Asindexptions:
//    --
//-----------------------------------------------------------------------------
#include "hashmap.h"
#include "comparable.h"
#include <sstream>

//-----------------------------------------------------------------------------
// Constructor
HashMap::HashMap(const int size) : TABLESIZE(size*4) {
  hashTable = new Comparable*[TABLESIZE];

  for (int i = 0; i < TABLESIZE; i++) {
    hashTable[i] = NULL;
  }
}

//-----------------------------------------------------------------------------
// Destructor
HashMap::~HashMap(void) {
  for (int i = 0; i < TABLESIZE; i++) {
    delete hashTable[i];
    hashTable[i] = NULL;
  }
  delete [] hashTable;
  hashTable = NULL;
}

//-----------------------------------------------------------------------------
// Add
// Add takes a pre-populated factory object and uses the unique identifying
// information; such as title, author, and year; and passes that to the hash
// function. Hash returns an index which is used to place the object in the
// hash table. Add then also inserts into a binary tree, which means objects
// must be comparable.
const bool HashMap::add(Comparable* comparableObject) {
  // Get unique identifier from factory object, hash it and return index. If
  // array at index is full, hash again with i until bucket is NULL or i
  // exceeds table size
  string unique = comparableObject->getUnique();  // fetch from object once
  int index = hash(unique);                          // hash for first index
  int i;
  // if bucket is full at index, increase i
  for (i = 0; hashTable[index] != NULL && i < TABLESIZE; i++)
    index = hash(unique, i);

  if (i == TABLESIZE) // means insert failed because table array was full
    return false;

  hashTable[index] = comparableObject;
  return true;
}

//-----------------------------------------------------------------------------
// Hash
// Using double hash method. Passing copy of istream because may need to be
// used again for hashing n...TABLESIZE times.
const int HashMap::hash(string unique, int i) const {
  int x,h1,h2;
  x = h1 = h2 = 0;        // initialization removes errors in valgrind

  // sum decimal equivalent of ASCII chars in unique
  for (int j = 0; j < unique.length(); j++)
    x += unique[j];

  h1 = x % TABLESIZE;     // h1(x) = x % TABLESIZE, where x is sum
  h2 = 7 - (x % 7);       // h2(x) = r - (x % r), where 'r' is prime
  return h1 + i * h2;     // index = h1(x) + i * h2(x), where 'i' is index
}

//-----------------------------------------------------------------------------
// Retrieve
Comparable* HashMap::retrieve(Comparable* key) const {
  // Get unique identifier from factory object, hash it and return index. If
  // array at index is full, hash again with i until bucket is NULL or i
  // exceeds table size
  string unique = key->getUnique();
  //***************************************************************************
  cout << "HashMap::retrieve unique = " << unique << endl;
  //***************************************************************************
  int index = hash(unique);                          // hash for first index
  int i;
  // look at index for object
  // if object at bucket is not key and bucket is not NULL, increase i
  for (i = 0; hashTable[index] != NULL &&
                  *hashTable[index] != *key &&
                              i < TABLESIZE;) {
  //***************************************************************************
  cout << "HashMap::retrieve index = " << index << endl;
  //***************************************************************************
  //***************************************************************************
  // if (hashTable[index] != NULL)
  //   cout << "HashMap::retrieve hashTable[index] = " << hashTable[index] << endl;
  //***************************************************************************
    index = hash(unique, i++);
  //***************************************************************************
  cout << "HashMap::retrieve index = " << index << endl;
  //***************************************************************************
  //***************************************************************************
  // if (hashTable[index] != NULL)
  //   cout << "HashMap::retrieve hashTable[index] = " << hashTable[index] << endl;
  //***************************************************************************
  }
  return 0;//hashTable[index];
}
