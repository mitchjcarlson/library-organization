//-----------------------------------------------------------------------------
// history.h
// History class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// History class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef HISTORY_H
#define HISTORY_H
#include "command.h"

class Patron;

class History : public Command {

public:
  History(const char = 'H');
  virtual ~History(void);
  virtual FactoryObject* create(void) const;
  virtual const bool populate(istream&);
  // virtual string getUnique(void) const {}
  virtual ostream& run(ostream&,LibraryManager&);

protected:
  Patron* patronKey;

}; // end History
#endif
