//-----------------------------------------------------------------------------
// factory.cpp
// Factory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Factory class: Generic factory super class, for creating factory objects and
//                returning a pointer to the new object. Creates an object by
//                utilizing a hash function. Hashing creates a subscript for
//                an array, which points to an object. Objects must implement
//                create(). Create() is called on the object and returned to
//                user.
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "itemfactory.h"
#include "fiction.h"
#include "periodical.h"
#include "children.h"
//-----------------------------------------------------------------------------
// Constructor
ItemFactory::ItemFactory(void) : Factory() { // Fact() will init. array w/NULL
  factoryObject[5] = new Fiction();
  factoryObject[15] = new Periodical();
  factoryObject[24] = new Children();
}
