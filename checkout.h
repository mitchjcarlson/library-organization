//-----------------------------------------------------------------------------
// checkout.h
// Checkout class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Checkout class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef CHECKOUT_H
#define CHECKOUT_H
#include "command.h"

class ItemFactory;
class Patron;
class Item;

class Checkout : public Command {

public:
  Checkout(const char = 'C');
  virtual ~Checkout(void);

  virtual FactoryObject* create(void) const;
  virtual const bool populate(istream&);
  // virtual string getUnique(void) const;
  virtual ostream& run(ostream&,LibraryManager&);

protected:
  int patronID;
  char itemType, edition;
  Patron* patronKey;
  Item* itemKey;
  Patron* patron;
  Item* item;
  ItemFactory itemFactory;
  virtual ostream& print(ostream&) const;

}; // end Checkout
#endif
