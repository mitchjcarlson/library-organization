//-----------------------------------------------------------------------------
// commandfactory.cpp
// Factory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Factory class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "commandfactory.h"
#include "checkout.h"
#include "display.h"
#include "history.h"
#include "return.h"
#include <iostream>
using namespace std;
//-----------------------------------------------------------------------------
// Constructor
CommandFactory::CommandFactory(void) : Factory() { // <- init. array to NULL
  factoryObject[2] = new Checkout();
  factoryObject[3] = new Display();
  factoryObject[7] = new History();
  factoryObject[17] = new Return();
}
