//-----------------------------------------------------------------------------
// factoryobject.h
// FactoryObject class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// FactoryObject class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "factoryobject.h"

#include <ostream>
using namespace std;

FactoryObject::FactoryObject(const char type) : TYPE(type) {}

//-----------------------------------------------------------------------------
// Operator <<
// ostream& operator<<(ostream& out,const FactoryObject& rhs) {
//   rhs.print(out);
//   return out;
// }
