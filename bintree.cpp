//-----------------------------------------------------------------------------
// BINTREE.CPP
// Binary search tree class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// BinTree class: Nodes point to item containers (itemNodes) that hold values
//                that are comparable. Data nodes are stored inorder from least
//                to greatest.
//  Functionality includes:
//    --Comparing two trees for equality
//    --Retrieve pointers to itemNodes based on item
//    --Get the depth of a node based on item
//    --Transfer itemNodes to an array from the tree
//    --Build balanced tree from array
//    --Check if the tree is empty
//    --Make a tree empty
//    --Insert nodeData into the tree creating a new node
//    --visually display the tree sideways
//
//  Assumptions:
//    --pointers returned by retrieve will not be used to insert NULL values
//    --getDepth works for any binary tree, it does not assume it is sorted
//    --insert method wont be passed a NULL nodeData value
//    --bstreeToArray wont be called on trees with greater than 100 nodes
//-----------------------------------------------------------------------------
#include "bintree.h"
#include "item.h"

//-----------------------------------------------------------------------------
// Default constructor
// Initializes an empty tree object.
BinTree::BinTree(void) : root(NULL) {} // end of default constructor

//-----------------------------------------------------------------------------
// Copy constructor
// Deep copy. Takes tree object as parameter and initializes a new tree object
// with the same item and structure as the tree object parameter.
BinTree::BinTree(const BinTree &other)
{
  // Note to self: Do not use insert.
  root = copyHelper(other.root);
}

// copyHelper
// Used by copy constructor and assignment operator. Returns a Node for root
// to point at. Each recursive call builds Node to be pointed at, and
// recursively builds subtrees until current is NULL.
BinTree::Node* BinTree::copyHelper(Node* current)
{
  if ( current != NULL )
  {
    Node *newNode = new Node;                    // tree node

    // send to create factory object
    // stringstream infile;
    // infile << *current->item;
    // char firstChar = infile.peek();             // first character e.g. F, Y, P
    // newNode->item = itemFactory.createObject(firstChar);
    // if (item != NULL) {
    //   item->populate(infile);      // input data to item

    // newNode->item = new Item; // item container
    // *newNode->item = *current->item;             // deep copy
    newNode->left  = copyHelper(current->left);  // copy left subtree
    newNode->right = copyHelper(current->right); // copy right subtree
    return newNode;
  }

  return NULL; // base case
}

//-----------------------------------------------------------------------------
// Destructor
// Free up memory block allocation
BinTree::~BinTree(void)
{
  makeEmpty();
}

//-----------------------------------------------------------------------------
// Assignment operator
// Deep copy. Assigns lhs tree the same item and structure as rhs tree.
BinTree& BinTree::operator=(const BinTree &rhs)
{
  if ( this != &rhs )               // Check if same mem address.
  {
    if ( root != NULL )
      makeEmpty();                  // No, then delete old
    root = copyHelper(rhs.root);    // get new
  }

  return *this;
}

//-----------------------------------------------------------------------------
// Equality operator
// Returns true if lhs tree has same item and same structure as rhs tree.
bool BinTree::operator==(const BinTree &rhs) const
{
  return equality(root, rhs.root);
}

// equalityHelper
// Preorder traversal of two trees simultaniously, checking the roots for item
// equality and then recursively checking the item of the roots of the subtrees
// for equality. If the roots are the same, and the roots of each subsequent
// subtree are the same, then the two trees are the same.
bool BinTree::equality(const Node* lhsCurrent, const Node* rhsCurrent) const
{
  if ( lhsCurrent != NULL && rhsCurrent != NULL )        // Avoid memory errors
  {
    if ( lhsCurrent->item != rhsCurrent->item )                 // check item
      return false;

    if (equality(lhsCurrent->left, rhsCurrent->left) && // check subtrees
        equality(lhsCurrent->right,rhsCurrent->right))
      return true;
  }
  else if ( lhsCurrent == NULL && rhsCurrent != NULL ||      // check structure
            lhsCurrent != NULL && rhsCurrent == NULL )
    return false;
  return true;                  // both pointers are NULL, so they are the same
}

//-----------------------------------------------------------------------------
// Inequality operator
// Returns true if lhs tree has either different item or different structure as
// hrs tree.
bool BinTree::operator!=(const BinTree& rhs) const
{
  return !(*this == rhs);
}

//-----------------------------------------------------------------------------
// Output stream operator
// Displays tree as an inorder traversal.
ostream& operator<<(ostream& out, const BinTree& rhs)
{
  rhs.inorderHelper(out, rhs.root);
  return out;
}

// inorderHelper
// Helper method for the output stream operator. It makes recursive calls down
// the left subtree until NULL, then outputs the item member, and then makes
// another recursive call to the right subtree.
ostream& BinTree::inorderHelper( ostream& out, Node* current ) const
{
  if ( current != NULL )
  {
    inorderHelper( out, current->left ); // return item of left subtree
    out << *current->item << endl;        // current Node item to output
    inorderHelper( out, current->right );// return item of right subtree
  }
  return out;
}

//-----------------------------------------------------------------------------
// retrieve
// Returns true if the searchKey parameter is found. If searchKey is found,
// refToData points to the memory address of the item that matches searchKey.
bool BinTree::retrieve(const Item &searchKey, Item *&refToData) const
{

  return retrieveHelper(searchKey, refToData, root);
}

// retrieveHelper
// Helper method for retrieve. Recursively compares searchKey to item,
// exploring left subtree is searchKey is lesser than, right subtree if
// searchKey is greater. Returns true if found and refToData points at itemNode
bool BinTree::retrieveHelper
  (const Item &searchKey, Item *&refToData, Node* current) const
{
  if ( current != NULL )
  {
    if ( searchKey < *current->item )       // go left subtree
      return retrieveHelper(searchKey, refToData, current->left);
    else if ( searchKey > *current->item )  // go right subtree
      return retrieveHelper(searchKey, refToData, current->right);
    else                                    // searchKey is equal
    {
      refToData = current->item; // point at whatever searchKey is pointing at
      return true;
    }
  }
  return false;                             // current == NULL, didn't find it
}

//-----------------------------------------------------------------------------
// getDepth
// Returns the depth of a node whose value is equal to the item parameter.
// This function operates on the assumption that the nodeData is not in any
// particular order in a binary tree. Depth at root is one.
int BinTree::getDepth(const Item &item) const
{
  return depth(item, root, 1); // delete this
}

// depth
// Helper method for getDepth(). Searches tree in preorder.
int BinTree::depth
  ( const Item &item, const Node *current, int level ) const
{
  if ( current != NULL )
  {
    if ( item == *current->item )      // found item, return level
      return level;

                                       // check left subtree
    int leftSubtree  = depth( item, current->left, level+1 );
    if ( leftSubtree )
      return leftSubtree;

                                       // check right subtree
    return depth( item, current->right, level+1 );
  }
  return 0;                            // if NULL, didn't find item
}

//-----------------------------------------------------------------------------
// bstreeToArray
// This function fills an array of Item by using inorder traversal of tree.
// It leaves the tree empty. It statically allocates an array of 100 NULL
// elements, and assumes that no size error checking is necessary.
void BinTree::bstreeToArray(Item* itemArray[])
{
  int index = 0;
  toArray( root, itemArray, index );
  makeEmpty();
} // end bstreeToArray

// toArray
// bstreeToArray helper method. It places values in an array from a tree.
void BinTree::toArray(Node* current, Item* itemArray[], int& index )
{
  if ( current != NULL )
  {
    toArray( current->left, itemArray, index );   // explore left subree
    itemArray[index++] = current->item;           // point at item node
    current->item = NULL;
    toArray( current->right, itemArray, index );  // explore  right subree
    //delete current;
  }

} // end toArray

//-----------------------------------------------------------------------------
// arrayToBSTree
// Builds a balanced tree from a sorted array of Item leaving the array
// filled with NULLs. The root (recursively) is at (low+high)/2 where low is
// the lowest subscript of the array range and high is the highest.
void BinTree::arrayToBSTree(Item* itemArray[])
{
  int size = 0;
  while (itemArray[size] != NULL )
    size++;
  root = toBSTree(itemArray, root, size-1, 0);
} // end arrayToBSTree

// toBSTree
// Helper method for arrayToBSTree. Recursively makes mid the root and builds
// a left subtree from the values less than mid and a right subtree from the
// values greater than mid.
BinTree::Node* BinTree::toBSTree
  ( Item* itemArray[], Node* current, int max, int min )
{
  int mid = (max+min)/2;

  if ( mid <= max && mid >= min )   // bounds checking
  {
    Node *newNode = new Node;       // new node
    newNode->item = itemArray[mid]; // reassign value
    itemArray[mid] = NULL;          // clear array value
    // left of mid = left sub
    newNode->left = toBSTree(itemArray, newNode->left, mid-1, min);
    // right of mid = right sub
    newNode->right = toBSTree(itemArray, newNode->right, max, mid+1);
    return newNode;              // link new node to previous by pointer
  }
  return NULL;
} // end toBSTree

//-----------------------------------------------------------------------------
// isEmpty
// Returns true if the root does not point to a tree.
bool BinTree::isEmpty() const
{
  return (root == NULL);
} // end isEmpty

//-----------------------------------------------------------------------------
// makeEmpty
// Deletes all tree nodes.
void BinTree::makeEmpty()
{
  empty(root);
  root = NULL;
}

// empty
// Helper function for makeEmpty. Recursively explores left and right subtrees,
// deleting itemNodes and Nodes until root is NULL.
void BinTree::empty(Node* current)
{
  if ( current != NULL )
  {
    empty(current->left);             // explore left
    empty(current->right);            // explore right
    delete current;                   // delete Node
    current = NULL;
  }
}

//-----------------------------------------------------------------------------
// insert
// Places a node in the tree, inorder.
bool BinTree::insert(Item* item)
{
  bool inserted = false; // required by method buildTree in driver
  root = insertHelper(root, item, inserted); // inserts item
  return inserted;
}

// insertHelper
// Recursively walks the tree, comparing item values until the value is found
// or a NULL value is found. If NULL, a new Node is created in the heap and
// returned to either the left or right pointer of the node that the method was
// called from in the previous recursion.
BinTree::Node* BinTree::insertHelper
  ( Node* current, Item* item, bool &inserted) {
  if ( current == NULL )      // This is where the new value will be inserted
  {
    Node *newNode = new Node;
    newNode->item = item;
    newNode->left = NULL;     // prevent memory access errors
    newNode->right = NULL;
    inserted = true;          // used by driver to build tree
    return newNode;
  }
  else                                 // Not there yet, keep going
  {
    if ( *item < *current->item )      // go left
      current->left = insertHelper( current->left, item, inserted );
    else if ( *item > *current->item ) // go right
      current->right = insertHelper( current->right, item, inserted );
  }
  return current;// returns node back to cursively called previous node pointer
}

//-----------------------------------------------------------------------------
// displaySideways
// Displays a binary tree as though you are viewing it from the side;
// hard coded displaying to standard output.
void BinTree::displaySideways() const {
   sideways(root, 0);
}

void BinTree::sideways(Node* current, int level) const {
   if (current != NULL) {
      level++;
      sideways(current->right, level);

      // indent for readability, 4 spaces per depth level
      for(int i = level; i >= 0; i--) {
          cout << "    ";
      }

      cout << *current->item << endl;        // display information of object
      sideways(current->left, level);
   }
}
