//-----------------------------------------------------------------------------
// return.cpp
// Return class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Return class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "return.h"

//-----------------------------------------------------------------------------
// Constructor
Return::Return(void) {}

//-----------------------------------------------------------------------------
// Create
FactoryObject* Return::create(void) const {
  return new Return();
}


//-----------------------------------------------------------------------------
// Populate
const bool Return::populate(istream& infile) {

  // string bookCode;
  // infile >> bookCode;             // reads the char code

  // infile.get();                   // get (and ignore) blank before author
  // getline(infile, title, ',');    // input author, looks for comma terminator

  // infile >> month;
  // infile >> year;                 // input year
}

ostream& Return::run(ostream& out, LibraryManager& libraryManager) {
  return out;
}
