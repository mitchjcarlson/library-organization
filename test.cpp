#include "librarymanager.h"
#include "factoryobject.h"
#include <sstream>
// #include <iostream>
// using namespace std;

// struct A
// {
//   A(int n) : a(n){};
//   int a;
// };

int main(){

  // A** ptr = new A*[10];
  // for (int i = 0; i < 10; i++)
  //   *(ptr + i) = new A(i);

  // for (int i = 0; i < 10; i++) {
  //   cout << (**(ptr + i)).a << endl;
  //   cout << (*ptr[i]).a << endl;
  // }

  LibraryManager lib;
  lib.buildInventory("data4books.txt");
  lib.buildUsers("data4patrons.txt");
  lib.runCommands("data4commands2.txt");

  return 0;
}
