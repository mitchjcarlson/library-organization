//-----------------------------------------------------------------------------
// patronfactory.cpp
// PatronFactory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// PatronFactory class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "patronfactory.h"
#include "patron.h"
#include "factoryobject.h"
// #include <stddef.h>
// #include <ostream>


//-----------------------------------------------------------------------------
// Constructor
PatronFactory::PatronFactory(void) : Factory() { // init. array w/NULL
  factoryObject[15] = new Patron();
}
