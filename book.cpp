//-----------------------------------------------------------------------------
// book.cpp
// Book class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Book class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "book.h"

// Default constructor
Book::Book(const char type) : Item(MAX_BOOK,type) {}

const bool Book::populate(istream& infile) {

  infile.get();                   // get (and ignore) blank before author
  getline(infile, author, ',');   // input author, looks for comma terminator

  infile.get();                   // get (and ignore) blank before author
  getline(infile, title, ',');   // input title, looks for comma terminator

  if(infile.eof())
    return false;

  infile >> year;                 // input year
  infile.get();                   // get (and ignore) end-of-line character

  return true;
}

ostream& Book::print(ostream& out) const {
  out << " " << setw(AVAIL_COL_WIDTH) << available
             << setw(AUTHOR_COL_WIDTH) << author
             << setw(TITLE_COL_WIDTH) << title
             << setw(YEAR_COL_WIDTH) << year;
  return out;
}
