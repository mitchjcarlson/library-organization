//-----------------------------------------------------------------------------
// item.cpp
// Item class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Item class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "item.h"
#include "patron.h"
#include <string>
using namespace std;

//-----------------------------------------------------------------------------
// Default constructor
Item::Item(int max, const char type)
      : maxItem(max), available(max), FactoryObject(type), Comparable() {
  // for(int i = 0; i < maxItem; i++)
  //   patrons[i] = NULL;           // initialize array to NULL
}

//-----------------------------------------------------------------------------
// Destructor
Item::~Item() {
  // for(int i = 0; i < maxItem; i++) {
  //   delete patrons[i];
  //   patrons[i] = NULL;
  // }
}

const bool Item::checkoutItem() {
  if (available) {
    available--;
    return true;
  }
  return false;
}

const bool Item::returnItem() {
  if (available < maxItem) {
    available++;
    return true;
  }
  return false;
}

