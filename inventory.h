//-----------------------------------------------------------------------------
// inventory.h
// Inventory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Inventory class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef INVENTORY_H
#define INVENTORY_H
#include "hashmap.h"
#include "bintree.h"
#include "itemfactory.h"

class Comparable;              // forward declaration

class Inventory : public HashMap {

public:
  Inventory(const int = 199);           // constructor

  virtual const bool add(Comparable*);  // single object
  BinTree fictionTree;
  BinTree childrensTree;
  BinTree periodicalsTree;

};
#endif
