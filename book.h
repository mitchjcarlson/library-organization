//-----------------------------------------------------------------------------
// book.h
// Book class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Book class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef BOOK_H
#define BOOK_H
#include <string>
#include "item.h"

const int AVAIL_COL_WIDTH = 5;
const int AUTHOR_COL_WIDTH = 25;
const int TITLE_COL_WIDTH = 35;
const int YEAR_COL_WIDTH = 4;
const int MAX_BOOK = 5;

class Book : public Item {

public:
  Book(const char = '\0');  // constructor(author)

  virtual FactoryObject* create(void) const = 0;
  virtual const bool populate(istream&);
  // virtual string getUnique(void) const = 0;

protected:
  string author;
  string title;
  virtual ostream& print(ostream&) const;

}; // end Book
#endif
