//-----------------------------------------------------------------------------
// display.cpp
// Display class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Display class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "display.h"
#include "inventory.h"
#include <iomanip>

//-----------------------------------------------------------------------------
// Constructor
Display::Display(const char type) : Command(type) {}

//-----------------------------------------------------------------------------
// Create
FactoryObject* Display::create(void) const {
  return new Display();
}

ostream& Display::run(ostream& out, LibraryManager& libraryManager) {
  out << "Fiction:" << endl;
  out << left << setw(AVAIL_COL_WIDTH) << "AVAIL"
               << setw(AUTHOR_COL_WIDTH) << "AUTHOR"
               << setw(TITLE_COL_WIDTH) << "TITLE"
               << setw(YEAR_COL_WIDTH) << "YEAR" << endl;
  out << libraryManager.inventory->fictionTree << endl;

  out << "Childrens:" << endl;
  out << setw(AVAIL_COL_WIDTH) << "AVAIL"
       << setw(AUTHOR_COL_WIDTH) << "AUTHOR"
       << setw(TITLE_COL_WIDTH) << "TITLE"
       << setw(YEAR_COL_WIDTH) << "YEAR" << endl;
  out << libraryManager.inventory->childrensTree << endl;

  out << "Periodicals:" << endl;
  out << left << setw(6) << "AVAIL" << setw(5) << "YEAR"
       << setw(3) << "MO" << "TITLE" << endl;
  out << libraryManager.inventory->periodicalsTree << endl;
  return out;
}
