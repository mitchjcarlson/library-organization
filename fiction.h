//-----------------------------------------------------------------------------
// fiction.h
// Fiction class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Fiction class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef FICTION_H
#define FICTION_H
#include "factoryobject.h"
#include "book.h"

class Fiction : public Book {

public:
  Fiction(const char = 'F');                              // constructor

  virtual FactoryObject* create(void) const;
  virtual const string getUnique(void) const;
  // virtual const char getType(void) const { return type; }

  virtual const bool operator<(const Comparable&) const;
  virtual const bool operator>(const Comparable&) const;
  virtual const bool operator<=(const Comparable&) const;
  virtual const bool operator>=(const Comparable&) const;
  virtual const bool operator==(const Comparable&) const;
  virtual const bool operator!=(const Comparable&) const;

}; // end Fiction

#endif
