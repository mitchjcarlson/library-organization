//-----------------------------------------------------------------------------
// inventory.cpp
// Inventory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Inventory class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "inventory.h"
#include "factoryobject.h"
#include "item.h"
#include <fstream>
#include <string>

//-----------------------------------------------------------------------------
// Constructor
Inventory::Inventory(const int size) : HashMap(size) {} // initialize table

//-----------------------------------------------------------------------------
// Add
const bool Inventory::add(Comparable* obj) {
  Item* item = static_cast<Item*>(obj);
  if (HashMap::add(item)) {
    if (item->getType() == 'F')
      fictionTree.insert(item);
    else if (item->getType() == 'Y')
      childrensTree.insert(item);
    else if (item->getType() == 'P')
      periodicalsTree.insert(item);
    else
      return false; // binary tree add was unsuccessful
    // else not in a category
    return true;  // add was successful
  }
  else
    return false; // add was unsuccessful
}
