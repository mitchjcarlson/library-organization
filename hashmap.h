//---------------------------------------------------------------------------
// hashmap.h
// HashMap class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// HashMap class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef HASHMAP_H
#define HASHMAP_H
#include <iostream>
using namespace std;

class Comparable;

class HashMap {

public:
  HashMap(const int = 199);           // constructor
  virtual ~HashMap(void);             // destructor

  virtual const bool add(Comparable*);
  virtual Comparable* retrieve(Comparable*) const;

protected:
  const int TABLESIZE;
  Comparable** hashTable;
  virtual const int hash(string,int=0) const;

}; // end HashMap
#endif
