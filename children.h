//-----------------------------------------------------------------------------
// children.h
// Children class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Children class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef CHILDREN_H
#define CHILDREN_H
#include "book.h"

class Children : public Book {

public:
  Children(const char = 'Y');                             // constructor

  virtual FactoryObject* create(void) const;
  virtual const string getUnique(void) const;
  // virtual const char getType(void) const { return type; }

  virtual const bool operator<(const Comparable&) const;
  virtual const bool operator>(const Comparable&) const;
  virtual const bool operator<=(const Comparable&) const;
  virtual const bool operator>=(const Comparable&) const;
  virtual const bool operator==(const Comparable&) const;
  virtual const bool operator!=(const Comparable&) const;

// protected:
//   const char type;

}; // end Children
#endif
