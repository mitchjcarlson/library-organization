//-----------------------------------------------------------------------------
// comparable.h
// Comparable class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Comparable class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef COMPARABLE_H
#define COMPARABLE_H
#include <string>
using namespace std;

class Comparable {

public:
  Comparable(void) : author(""), title(""), year(0), month(0), id(0) {}
  virtual ~Comparable(void) {}  // destructor

  virtual const string getAuthor(void) const { return author; }
  virtual const string getTitle(void) const { return title; }
  virtual const int getYear(void) const { return year; }
  virtual const int getMonth(void) const { return month; }
  virtual const int getID(void) const { return id; }

  virtual const bool operator<(const Comparable&) const = 0;
  virtual const bool operator>(const Comparable&) const = 0;
  virtual const bool operator<=(const Comparable&) const = 0;
  virtual const bool operator>=(const Comparable&) const = 0;
  virtual const bool operator==(const Comparable&) const = 0;
  virtual const bool operator!=(const Comparable&) const = 0;
  virtual const string getUnique(void) const = 0;

protected:
  string author;
  string title;
  int year;
  int month;
  int id;

}; // end Comparable
#endif
