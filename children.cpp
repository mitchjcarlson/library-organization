//-----------------------------------------------------------------------------
// children.cpp
// Children class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Children class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "children.h"
#include <sstream>

//-----------------------------------------------------------------------------
// Constructor
Children::Children(const char type) : Book(type) {}

//-----------------------------------------------------------------------------
// Create
FactoryObject* Children::create(void) const {
  return new Children();
}

const string Children::getUnique(void) const {
  stringstream unique;
  unique << title << " " << author;
  return unique.str();
}

//-----------------------------------------------------------------------------
// Operator <
const bool Children::operator<(const Comparable& rhs) const {
  return (title < rhs.getTitle()) ||
    (title == rhs.getTitle() && author < rhs.getAuthor());
}
//-----------------------------------------------------------------------------
// Operator >
const bool Children::operator>(const Comparable& rhs) const {
  return (title > rhs.getTitle()) ||
    (title == rhs.getTitle() && author > rhs.getAuthor());
}
//-----------------------------------------------------------------------------
// Operator <=
const bool Children::operator<=(const Comparable& rhs) const {
  return !(*this > rhs); // not greater than, so lesser than or equal to
}
//-----------------------------------------------------------------------------
// Operator >=
const bool Children::operator>=(const Comparable& rhs) const {
  return !(*this < rhs); // not lesser than, so greater than or equal to
}
//-----------------------------------------------------------------------------
// Operator ==
const bool Children::operator==(const Comparable& rhs) const {
  return title == rhs.getTitle() && author ==  rhs.getAuthor();
}
//-----------------------------------------------------------------------------
// Operator !=
const bool Children::operator!=(const Comparable& rhs) const {
  return !(*this == rhs);
}
