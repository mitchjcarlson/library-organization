//-----------------------------------------------------------------------------
// librarymanager.h
// LibraryManager class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// LibraryManager class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef LIBRARYMANAGER_H
#define LIBRARYMANAGER_H
#include "itemfactory.h"
#include "commandfactory.h"
#include "hashmap.h"
#include "inventory.h"
#include "patronfactory.h"

class LibraryManager {

public:
  LibraryManager(void) {}
  virtual ~LibraryManager(void);
  virtual void buildInventory(const char*);
  virtual void buildUsers(const char*);
  virtual void runCommands(const char*);
  Inventory* inventory;
  HashMap* users;

protected:
  ItemFactory itemFactory;
  PatronFactory patronFactory;
  CommandFactory commandFactory;
  const bool endOfFile(istream&,char) const;

}; // end LibraryManager

#endif
