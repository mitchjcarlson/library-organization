//-----------------------------------------------------------------------------
// patron.cpp
// Patron class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Patron class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "patron.h"
#include <sstream>

//-----------------------------------------------------------------------------
// Default constructor
Patron::Patron(int id, string l, string f)
          : id(id),lastName(l),firstName(f){} // patron doesn't have a type

//-----------------------------------------------------------------------------
// Destructor
Patron::~Patron(void){}

FactoryObject* Patron::create(void) const {
  return new Patron;
}

const bool Patron::populate(istream& infile) {
  infile >> id;
  if(infile.good()) {
    infile >> lastName;
    infile >> firstName;
  }
}

const string Patron::getUnique(void) const {
  stringstream out;
  out << id;
  return out.str();
}

const bool Patron::checkoutItem(Item* item) {
  return history.pushFront(item, "Checkout");
}

const bool Patron::returnItem(Item* item) {
  return history.pushFront(item, "Return");
}

//-----------------------------------------------------------------------------
// Operator <
const bool Patron::operator<(const Comparable& rhs) const {
  return (id < rhs.getID());
}
//-----------------------------------------------------------------------------
// Operator >
const bool Patron::operator>(const Comparable& rhs) const {
  return (id > rhs.getID());
}
//-----------------------------------------------------------------------------
// Operator <=
const bool Patron::operator<=(const Comparable& rhs) const {
  return !(*this > rhs); // not greater than, so lesser than or equal to
}
//-----------------------------------------------------------------------------
// Operator >=
const bool Patron::operator>=(const Comparable& rhs) const {
  return !(*this < rhs); // not lesser than, so greater than or equal to
}
//-----------------------------------------------------------------------------
// Operator ==
const bool Patron::operator==(const Comparable& rhs) const {
  return (id == rhs.getID());
}
//-----------------------------------------------------------------------------
// Operator !=
const bool Patron::operator!=(const Comparable& rhs) const {
  return !(*this == rhs);
}
//-----------------------------------------------------------------------------
// Print
ostream& Patron::print(ostream& out) const {
  out << "*** Patron ID = " << id << "  "
      << lastName << " " << firstName << endl;
  history.print(out);
  return out;
}
