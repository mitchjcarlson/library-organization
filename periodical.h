//-----------------------------------------------------------------------------
// periodical.h
// Periodical class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Periodical class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef PERIODICAL_H
#define PERIODICAL_H
#include "item.h"

class Periodical : public Item {

public:
  Periodical(const char = 'P');                      // Constructor

  virtual FactoryObject* create(void) const;
  virtual const bool populate(istream&);
  virtual const string getUnique(void) const;
  // virtual const char getType(void) const { return type; }

  virtual const bool operator<(const Comparable&) const;
  virtual const bool operator>(const Comparable&) const;
  virtual const bool operator<=(const Comparable&) const;
  virtual const bool operator>=(const Comparable&) const;
  virtual const bool operator==(const Comparable&) const;
  virtual const bool operator!=(const Comparable&) const;

protected:
  virtual ostream& print(ostream&) const;

}; // end Periodical
#endif
