//-----------------------------------------------------------------------------
// patronfactory.h
// PatronFactory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// PatronFactory class:
//
// Functionality includes:
//    --
//
// Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef PATRONFACTORY_H
#define PATRONFACTORY_H
#include "factory.h"

class PatronFactory : public Factory {

public:
  PatronFactory(void);                        // constructor

}; // end PatronFactory

#endif
