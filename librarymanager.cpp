//-----------------------------------------------------------------------------
// librarymanager.cpp
// LibraryManager class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// LibraryManager class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "librarymanager.h"
#include "factoryobject.h"
#include "item.h"
#include "patron.h"
#include "command.h"
#include <fstream>
#include <sstream>
#include <algorithm>
using namespace std;

//----------------------------- Deconstructor ---------------------------------
LibraryManager::~LibraryManager(void) {
  delete inventory;
  delete users;
  inventory = NULL;
  users = NULL;
}

//----------------------------- buildInventory --------------------------------
void LibraryManager::buildInventory(const char* fileName) {
  ifstream infile(fileName);        // input stream from data file
  string line;
  int tableSize = count(istreambuf_iterator<char>(infile),
                                  istreambuf_iterator<char>(), '\n');
  inventory = new Inventory(tableSize);

  infile.seekg(0,infile.beg);
  if (!infile) {
    cout << "Input file: "<<fileName <<", could not be opened." << endl;
    return;
  }

  while (getline(infile,line)) {    // while not end of file
    char firstChar;
    istringstream lineStream(line);
    lineStream >> firstChar;

    Item* item = static_cast<Item*>(itemFactory.createObject(firstChar));
    if (item != NULL) {
      item->populate(lineStream);   // input data to item
      (*inventory).add(item);       // add item to inventory
    }
    else {
      string badData;               // bad data, throw it out
      getline(infile,badData);
    }
  }
}

//------------------------------- buildUsers ----------------------------------
void LibraryManager::buildUsers(const char* fileName) {
  ifstream infile(fileName);        // input stream from data file
  string line;
  int tableSize = count(istreambuf_iterator<char>(infile),
                                  istreambuf_iterator<char>(), '\n');
  users = new HashMap(tableSize);

  infile.seekg(0,infile.beg);
  if (!infile) {
    cout << "Input file: "<<fileName <<", could not be opened." << endl;
    return;
  }

  while (getline(infile,line)) {          // while not end of file
    istringstream lineStream(line);
    Patron* patron = static_cast<Patron*>(patronFactory.createObject('P'));
    patron->populate(lineStream);  // input data to item
    (*users).add(patron);             // add item to inventory
  }
}

//------------------------------ runCommands ----------------------------------
void LibraryManager::runCommands(const char* fileName) {
  ifstream infile(fileName);                    // input stream from data file
  string line;

  if (!infile) {
    cout << "Input file: "<< fileName <<", could not be opened." << endl;
    return;
  }

  while (getline(infile,line)) {                // while not end of file
    char firstChar;
    istringstream lineStream(line);
    lineStream >> firstChar;

    Command* command =
      static_cast<Command*>(commandFactory.createObject(firstChar));

    if (command != NULL && lineStream.good()) {
      command->populate(lineStream);      // input data to item
    }
    else {
      string badData;                           // bad data, throw it out
      getline(infile,badData);
      cout << "Invalid input data: " << badData << endl;
    }

    command->run(cout,*this);
    delete command;
    command = NULL;
  }
}
