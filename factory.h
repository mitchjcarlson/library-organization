//-----------------------------------------------------------------------------
// factory.h
// Factory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Factory class: Generic factory super class, for creating factory objects and
//                returning a pointer to the new object. Creates an object by
//                utilizing a hash function. Hashing creates a subscript for
//                an array, which points to an object. Objects must implement
//                create(). Create() is called on the object and returned to
//                user.
// Functionality includes:
//    --
//
// Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef FACTORY_H
#define FACTORY_H

class FactoryObject;                    // forward declaration
const int FACTORY_ARRAY_SIZE = 26;                 // hash table size, letters in alphabet

class Factory {

public:
  Factory(void);                        // constructor
  virtual ~Factory(void);               // destructor
  virtual FactoryObject* createObject(const char ch) const; // create object

protected:
  virtual const int hash(const char) const; // hash function
  FactoryObject* factoryObject[FACTORY_ARRAY_SIZE];       // object ptr array

}; // end Factory

#endif
