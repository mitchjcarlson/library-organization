//-----------------------------------------------------------------------------
// fiction.h
// Fiction class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Fiction class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "fiction.h"
#include <sstream>

//-----------------------------------------------------------------------------
// Constructor
Fiction::Fiction(const char type) : Book(type) {}

//-----------------------------------------------------------------------------
// Create
FactoryObject* Fiction::create(void) const {
  return new Fiction();
}

const string Fiction::getUnique(void) const {
  stringstream unique;
  unique << author << title;
  return unique.str();
}

//-----------------------------------------------------------------------------
// Operator <
const bool Fiction::operator<(const Comparable& rhs) const {
  return (author < rhs.getAuthor()) ||
    (author == rhs.getAuthor() && title < rhs.getTitle());
}
//-----------------------------------------------------------------------------
// Operator >
const bool Fiction::operator>(const Comparable& rhs) const {
  return (author > rhs.getAuthor()) ||
    (author == rhs.getAuthor() && title > rhs.getTitle());
}
//-----------------------------------------------------------------------------
// Operator <=
const bool Fiction::operator<=(const Comparable& rhs) const {
  return !(*this > rhs); // not greater than, so lesser than or equal to
}
//-----------------------------------------------------------------------------
// Operator >=
const bool Fiction::operator>=(const Comparable& rhs) const {
  return !(*this < rhs); // not lesser than, so greater than or equal to
}
//-----------------------------------------------------------------------------
// Operator ==
const bool Fiction::operator==(const Comparable& rhs) const {
  return author == rhs.getAuthor() && title ==  rhs.getTitle();
}
//-----------------------------------------------------------------------------
// Operator !=
const bool Fiction::operator!=(const Comparable& rhs) const {
  return !(*this == rhs);
}
