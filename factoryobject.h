//-----------------------------------------------------------------------------
// factoryobject.h
// FactoryObject class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// FactoryObject class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef FACTORYOBJECT_H
#define FACTORYOBJECT_H
#include <iostream>
#include <iomanip>
using namespace std;

class FactoryObject { // ADT

public:
  FactoryObject(const char = '\0');              // constructor
  virtual ~FactoryObject(void){}               // destructor

  virtual FactoryObject* create(void) const = 0;  // Pure virtual
  virtual const bool populate(istream&) {}
  virtual const char getType(void) const { return TYPE; }
  // friend ostream& operator<<(ostream&,const FactoryObject&);

protected:
  // virtual ostream& print(ostream&) const {}
  const char TYPE;

}; // end FactoryObject
#endif
