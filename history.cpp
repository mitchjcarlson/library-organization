//-----------------------------------------------------------------------------
// history.cpp
// History class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// History class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "history.h"
#include "patron.h"
#include <sstream>

//-----------------------------------------------------------------------------
History::History(const char type) : patronKey(NULL), Command(type) {}

//-----------------------------------------------------------------------------
History::~History(void) {
delete patronKey;
patronKey = NULL;
}

//-----------------------------------------------------------------------------
// Create
FactoryObject* History::create(void) const {
  return new History();
}

//-----------------------------------------------------------------------------
// Populate
const bool History::populate(istream& infile) {
patronKey = new Patron();
patronKey->populate(infile);
}

ostream& History::run(ostream& out, LibraryManager& librarymanager) {
  // Patron* patron =
  //   static_cast<Patron*>(librarymanager.users->retrieve(patronKey));
  // if(patron)
  //   out << *patron << endl;
  return out;
}
