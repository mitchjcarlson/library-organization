//-----------------------------------------------------------------------------
// patron.h
// Patron class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Patron class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef PATRON_H
#define PATRON_H
#include <string>
#include "factoryobject.h" // superclass
#include "comparable.h" // superclass
#include "printable.h" // superclass
#include "list.h" // because it's part of patron
using namespace std;

class Item; // forward dec for checkout and return

class Patron : public FactoryObject, public Comparable, public Printable {

public:
  Patron(int = 0,string = "", string = "");      //constructor
  virtual ~Patron(void);  // destructor

  virtual FactoryObject* create(void) const;
  virtual const bool populate(istream&);
  virtual const string getUnique(void) const;
  virtual const bool checkoutItem(Item*);
  virtual const bool returnItem(Item*);

  virtual const bool operator<(const Comparable&) const;
  virtual const bool operator>(const Comparable&) const;
  virtual const bool operator<=(const Comparable&) const;
  virtual const bool operator>=(const Comparable&) const;
  virtual const bool operator==(const Comparable&) const;
  virtual const bool operator!=(const Comparable&) const;
  virtual ostream& print(ostream&) const;

protected:
  int id;
  string firstName;
  string lastName;
  List history;

}; // end Patron

#endif
