//-----------------------------------------------------------------------------
// list.cpp
// List class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// List class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "list.h"
#include "item.h"

List::List(Item* item, string type) {
  if(item != NULL)
    front = new Node(item,type);
  else
    front = NULL;
}

List::~List(void) {
  makeEmpty(front);
}

void List::makeEmpty(Node* current) {
  if (current != NULL) {
    makeEmpty(current->next);
    // delete current->item;
    delete current;
    current = NULL;
  }
}

const bool List::pushFront(Item* item, string type) {
  if(item != NULL) {
    Node* node = new Node(item,type);
    node->next = front;
    front = node;
    return true;
  }
  return false;
}

ostream& List::print(ostream& out) const {
  Node* current = front;
  while (current) {
    out << current->type << " " << *current->item << endl;
    current = current->next;
  }
  return out;
}

