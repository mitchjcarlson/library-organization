//-----------------------------------------------------------------------------
// checkout.cpp
// Checkout class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Checkout class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "checkout.h"
#include "factoryobject.h"
#include "itemfactory.h"
#include "item.h"
#include "patron.h"
#include <sstream>

//-----------------------------------------------------------------------------
// Constructor
Checkout::Checkout(const char type) : Command(type), patronID(0), itemType('\0'), edition('\0'),
    patronKey(NULL), itemKey(NULL) {}

//-----------------------------------------------------------------------------
// Destructor
Checkout::~Checkout(void) {
  delete itemKey;
  delete patronKey;
  itemKey = NULL;
  patronKey = NULL;
}

//-----------------------------------------------------------------------------
// Create
FactoryObject* Checkout::create(void) const {
  return new Checkout();
}

//-----------------------------------------------------------------------------
// Populate
const bool Checkout::populate(istream& infile) {
  stringstream validID; // for patron's populate method, must be stream
  infile >> patronID >> itemType >> edition;  // reads in tokens
  validID << patronID;
  patronKey = new Patron(); // used to generate key to retrieve patron
  patronKey->populate(validID);

  // cout << patronKey->getUnique() << endl;

  itemKey = static_cast<Item*>(itemFactory.createObject(itemType)); // item key
  if (itemKey != NULL)
    itemKey->populate(infile);

  // cout << *itemKey << endl;
  // cout << itemKey->getUnique() << endl;
}

// string Checkout::getUnique(void) const {}

ostream& Checkout::run(ostream& out, LibraryManager& libraryManager) {
  // patron = static_cast<Patron*>(libraryManager.users->retrieve(patronKey));
  // item = static_cast<Item*>(libraryManager.inventory->retrieve(itemKey));

  // cout << *patron << endl;
  // cout << *item << endl;

  // if (patron && item && item->checkoutItem()) { // checkout updates avail Qty
  //   patron->checkoutItem(item); // links patron history to item
  // }
  return out;
}

ostream& Checkout::print(ostream& out) const {
  // out << "Checkout " << *item;
}
