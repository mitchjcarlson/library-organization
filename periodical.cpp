//-----------------------------------------------------------------------------
// periodical.cpp
// Periodical class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Periodical class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#include "periodical.h"
#include <sstream>

//-----------------------------------------------------------------------------
// Constructor
Periodical::Periodical(const char type) : Item(1,type) {}

//-----------------------------------------------------------------------------
// Create
FactoryObject* Periodical::create(void) const {
  return new Periodical();
}

//-----------------------------------------------------------------------------
// Populate
const bool Periodical::populate(istream& infile) {
  FactoryObject::populate(infile);

  infile.get();                   // get (and ignore) blank before author
  getline(infile, title, ',');    // input author, looks for comma terminator

  infile >> month;
  infile >> year;                 // input year
  infile.get();                    // get (and ignore) end-of-line character
}

const string Periodical::getUnique(void) const {
  stringstream unique;
  unique << year << month << title;
  // for (string temp; unique >> temp;) cout << temp << endl;
  return unique.str();
}

//-----------------------------------------------------------------------------
// Operator <
const bool Periodical::operator<(const Comparable& rhs) const {
  return (getYear() < rhs.getYear()) ||
          (getYear() == rhs.getYear()) && (getMonth() < rhs.getMonth()) ||
            (getYear() == rhs.getYear()) && (getMonth() == rhs.getMonth()) &&
              (getTitle() < rhs.getTitle());
}
//-----------------------------------------------------------------------------
// Operator >
const bool Periodical::operator>(const Comparable& rhs) const {
  return (getYear() > rhs.getYear()) ||
          (getYear() == rhs.getYear()) && (getMonth() > rhs.getMonth()) ||
            (getYear() == rhs.getYear()) && (getMonth() == rhs.getMonth()) &&
              (getTitle() > rhs.getTitle());
}
//-----------------------------------------------------------------------------
// Operator <=
const bool Periodical::operator<=(const Comparable& rhs) const {
  return !(*this > rhs); // not greater than, so lesser than or equal to
}
//-----------------------------------------------------------------------------
// Operator >=
const bool Periodical::operator>=(const Comparable& rhs) const {
  return !(*this < rhs); // not lesser than, so greater than or equal to
}
//-----------------------------------------------------------------------------
// Operator ==
const bool Periodical::operator==(const Comparable& rhs) const {
  return (getYear() == rhs.getYear()) &&
    (getMonth() == rhs.getMonth()) && (getTitle() == rhs.getTitle());
}
//-----------------------------------------------------------------------------
// Operator !=
const bool Periodical::operator!=(const Comparable& rhs) const {
  return !(*this == rhs);
}
//-----------------------------------------------------------------------------
// Print
ostream& Periodical::print(ostream& out) const {
  out << " " << setw(5) << available << setw(5) << year
      << right << setw(2) << month << " " << left << title;
  return out;
}
