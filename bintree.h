//-----------------------------------------------------------------------------
// BINTREE.H
// Binary search tree class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// BinTree class: Nodes point to data containers (dataNodes) that hold values
//                that are comparable. Data nodes are stored inorder from least
//                to greatest.
//  Functionality includes:
//    --Comparing two trees for equality
//    --Retrieve pointers to dataNodes based on data
//    --Get the depth of a node based on data
//    --Transfer dataNodes to an array from the tree
//    --Build balanced tree from array
//    --Check if the tree is empty
//    --Make a tree empty
//    --Insert nodeData into the tree creating a new node
//    --visually display the tree sideways
//
//  Assumptions:
//    --pointers returned by retrieve will not be used to insert NULL values
//    --getDepth works for any binary tree, it does not assume it is sorted
//    --insert method wont be passed a NULL nodeData value
//    --bstreeToArray wont be called on trees with greater than 100 nodes
//-----------------------------------------------------------------------------

#ifndef BINTREE_H
#define BINTREE_H

// #include "factoryobject.h"
#include <iostream>
using namespace std;

class Item;

class BinTree
{
  friend ostream& operator<<(ostream&, const BinTree&);

public:
  BinTree(void);                          // Default constructor
  BinTree(const BinTree &);               // Copy constructor
  ~BinTree(void);                         // Destructor

  BinTree& operator=(const BinTree&);    // Assignment operator
  bool operator==(const BinTree&) const; // Equality comparison operator
  bool operator!=(const BinTree&) const; // Inequality comparison operator

  bool retrieve(const Item&, Item*&) const; // get node
  int  getDepth(const Item&) const;  // return depth at any value in tree
  void bstreeToArray(Item* []);       // fills an array of Item*
  void arrayToBSTree(Item* []);       // builds balaced BinTree

  bool isEmpty() const;         // true if tree is empty, otherwise false
  void makeEmpty();             // make the tree empty so isEmpty returns true
  bool insert(Item*);
  void displaySideways() const;           // public interface

private:
  struct Node {
    Item* item;         // pointer to item object
    Node* left;                           // left subtree pointer
    Node* right;                          // right subtree pointer
  };

  Node* root;                             // root of the tree

  // utility functions
  ostream &inorderHelper( ostream&, Node* ) const; // helper for ostream
  void sideways(Node*, int) const;                 // private helper
  Node* insertHelper( Node*, Item*, bool& );   // helper for insert
  Node* copyHelper(Node*);                       // helper for copy constructor
  void empty(Node*);                             // helper for makeEmpty
  bool equality(const Node*, const Node*) const; // helper for comparison op
  bool retrieveHelper(const Item&, Item*&, Node*) const; // helper
  int depth(const Item&, const Node*, int ) const; // helper for getDepth
  void toArray( Node*, Item* [], int& );     // helper for bstreeToArray
  Node* toBSTree( Item* [], Node*, int, int );//helper for arryToBSTree
};

#endif
