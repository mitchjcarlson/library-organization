//-----------------------------------------------------------------------------
// item.h
// Item class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// Item class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef ITEM_H
#define ITEM_H
#include <istream>
#include "factoryobject.h"
#include "comparable.h"
#include "printable.h"
using namespace std;

// class Patron;                     // forward declaration
// const int MAXITEM = 5;

class Item : public FactoryObject, public Comparable, public Printable {

public:
  Item(int=0,const char = '\0');    // constructor(maxItem,title,year)
  virtual ~Item();                // destructor
  virtual const bool checkoutItem(); // true for checkout, false for return
  virtual const bool returnItem(); // true for checkout, false for return

protected:
  int maxItem;        // total # of copies owned by the library
  int available;      // # of copies currently available to be checked out

  // Array of pointers to Patrons used to track who has which copy.
  // Each index in the array corresponds to a physical copy of the item.
  // NULL corresponds to a copy currently available for checkout.
  // Patron *patrons[MAXITEM]; // who checked it out

}; // end Item

#endif
// display
// history
// return
//
