//-----------------------------------------------------------------------------
// commandfactory.h
// ItemFactory class
// Author: Mitch Carlson
//-----------------------------------------------------------------------------
// CommandFactory class:
//
//  Functionality includes:
//    --
//
//  Assumptions:
//    --
//-----------------------------------------------------------------------------
#ifndef COMMANDFACTORY_H
#define COMMANDFACTORY_H
#include "factory.h"

class CommandFactory : public Factory {

public:
  CommandFactory(void); // constructor

}; // end ItemFactory

#endif
